import sys, math, string
from random import shuffle

# Print the tasting matrix
def print_matrix(matrix):
    for player in matrix:
        print(repr(player))

# Print a table showing the players sample IDs for every bottle
def print_tasting_table(bottles, matrix, segment_size):
    for bottle in range(len(bottles)):
        if bottle%segment_size == 0 and bottle != 0:
            print()
        print(bottles[bottle].ljust(len(max(bottles,key=len)))," \t",end="")
        for player in matrix:
            print(player[bottle],"\t",end="")
        print()

# Print a table for a player showing the bottles in their tasting order
def generate_comparison_table(bottles, player):
    # Note: This is slow and inefficient
    table = []
    for sample in [chr(65+i) for i in range(len(player))]:
        for i in range(len(player)):
            if sample == player[i]:
                table.append((sample, bottles[i]))
    return table
    

# CAREFUL: CURRENTLY DOES NOT WORK, RETURNS VERY WRONG RESULT
def generate_comparison_table_new(bottles, player):
    # combine samples with bottle names and sort by tasting order
    table = [(sample,bottles[ord(sample)-65]) for sample in player]
    table.sort(key= lambda x: x[0])
    return table

def print_comparison_table(table, segment_size):
    for i, entry in enumerate(table):
        if i != 0 and i%segment_size == 0:
            print()
        sample, bottle = entry
        print(sample, ":\t", bottle)


########
# Main
########

bottles = [bottle.rstrip() for bottle in open("bottles.txt").readlines()]
bottles_no = len(bottles)
shuffle(bottles)
players_no = 6
segment_size = 5

# Generate sorted matrix
matrix = [[chr(i+65) for i in range(bottles_no)] for a in range(players_no)]

# Shuffle matrix 
for player in matrix:
    # Shuffle one segment at a time
    # so everyone tries [A,B,C,D,E] and [F,G,H,I,J] 
    # and so on at the same time
    for segment_start in range(0,bottles_no,segment_size):
        segment_end = min((segment_start+segment_size,bottles_no))
        copy = player[segment_start:segment_end]
        shuffle(copy) 
        player[segment_start:segment_end] = copy

# Print tasting table
print_tasting_table(bottles, matrix, segment_size)

# Print comparison table for players
for i, player in enumerate(matrix):
    print()
    print("Player ", i, "\n=====")
    table = generate_comparison_table(bottles, player)
    print_comparison_table(table, segment_size)

